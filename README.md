# Simple_Sick_Camera

This Lib is only good for taking single pictures with the Camera by _Sick_. I've tested this one only on _Ubuntu 20.04_ (where i even had _Anaconda_ installed) and on _RaspbianOS_ and with the Camera _I2D304C-RCA11_. It probably works just fine with other Cameras of _Sick_ or _IDS_ and even with _Windows_ but i didn't test this.

**Preperations**
--------------

**Install IDS Software Suit**

First you should download and install the _IDS Software Suit_ ([Website](https://en.ids-imaging.com/download-details/AB.0010.1.56000.24.html?os=linux_arm&version=v7&bus=32&floatcalc=hard)). But be aware to install the right version. On _Linux_ with `uname -a` you get the information which version you need. For _RaspbianOS_ you need to take one of the _LinuxARM_ versions. Wether you need hard or soft floating point is described quite good in the readme of the _IDS_ software. Likewise is in the same readme the installation guide for the software.

**Make the IDS Software Suit work**

For Ubuntu and probably any desktop-distribution you don't need to do anything more in order to get the software work. Nonetheless check if the software is realy working. Plug in your camera and start the _IDS Camera Manager_ `sudo idscameramanager`. Configure your camera so you can actually use it. Don't worry the software makes sure you know, if you can use the camera or not. For further instructions check the readme of the _IDS  Software Suit_. Afterwards start the _IDS_ demo with the command `ueyedemo`. Try to take picture, if you get the picture, everything should be alright.

For _RaspbianOS_ there are few more things to be done before you can actually test the software. You should install the following:
- `sudo apt-get install qt5-default`
- `sudo apt-get install mesa-utils`

After the installations you need to make a configuration. For that run `sudo raspi-config`, go to _Advanced Options_ and choose _GLDrivers_. This should do and you would be able to use the software.

**Install Python Packages**
- `pip install pyueye opencv-python`
- for Python3 `sudo apt-get install python3-pyqt5` / for Python2 `sudo apt-get install python-pyqt5`
- only for _RaspbianOS_: `sudo apt-get install libatlas-base-dev` in order to get opencv running

**Don't forget to clone this repository**
`git clone https://git.rwth-aachen.de/juergen.janzen.de/simple_sick_camera.git`

**Usage**
--------------
The usage.py shows as example how to use this lib. It's quite straightforward, at least i think it is. The idea behind this whole thing is, that you use the _IDS SOftware Suit_ to configure your camera image ($`ueyedemo`) and save the configuration as _.ini_. With this and the lib you can make single shots.
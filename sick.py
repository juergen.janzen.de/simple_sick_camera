from pyueye import ueye
from camera import Camera
import cv2

class Sick(Camera):

    def __init__(self):
        super().__init__(device_id=0, buffer_count=1)

    def start(self, config_file=None):
        """
        Has to be executed before any other function.
        Starts the the camera with a specific configuration.
        """
        self.init() # actually starts the camera
        
        if config_file != None:
            pParam = ueye.wchar_p() # generates an config object
            pParam.value = config_file # gives the values 
                                    # of the config to the actuell object
            # set the config
            ueye.is_ParameterSet(self.h_cam, ueye.IS_PARAMETERSET_CMD_LOAD_FILE, pParam, 0)

    def stop(self):
        """
        Proper closing of the Sick Camera
        """
        self.exit()

    def data(self):
        """
        Takes a picture
        returns numpy-array
        """
        return self.capture_image()

    def saveImg(self, path):
        """
        Gets Data from Camera and saves a picture.
        path = './test/test2.jpeg'
        """

        pic = self.data()
        cv2.imwrite(path, pic)


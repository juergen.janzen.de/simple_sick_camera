from sick import Sick

# getting started
cam = Sick()
cam.start('picoCam-304C_conf.ini')

# get the pic as numpy array
nparr = cam.data()
print(nparr)

# save the pic as jpeg
cam.saveImg('test.jpeg')

# clean closing
cam.stop()